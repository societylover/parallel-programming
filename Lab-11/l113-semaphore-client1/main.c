#include <stdio.h>
#include <sys/msg.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>

const short OK1 = 2;

int main()
{
    int msqid, shmid;
    char pathname[] = "msgqueue.m";
    int len, i = 0, maxlen = 81;

    int semaphore = 0;

    int new = 0;

    key_t key;

    int *array;

    struct msgbuf {
        long mtype;
        struct {
            uint32_t sinfo;
            float finfo;
        } info;
    } buffer;

    buffer.mtype = 1; // Initial value

    if ((key = ftok(pathname, 0)) < 0) {
        printf("Can't generate key!\n");
        exit(-1);
    }

    if ((msqid = msgget(key, 0666 | IPC_CREAT)) < 0) {
        printf("Can't get msgqid!");
        exit(-1);
    }

    if ((shmid = shmget(key, 3*sizeof(int), 0666 | IPC_CREAT | IPC_EXCL)) < 0) {

        if (errno != EEXIST) {
            printf("Can't create shared memory!\n");
            exit(-1);
        } else {
            if ((shmid = shmget(key, 3*sizeof(int), 0)) < 0) {
                printf("Can't find shared memory!\n");
                exit(-1);
            }
            new = 0;
        }
    }


    if ((array = (int *)shmat(shmid, NULL, 0)) == (int*)(-1)) {
        printf("Can't attach shared memory!\n");
        exit(-1);
    }

    if (new) {
        array[0] = 1;
        array[1] = 0;
        array[2] = 1;
    } else {
        array[0] += 1;
        array[2] += 1;

        len = sizeof (buffer);

        buffer.mtype = OK1;

        if (msgsnd(msqid, (struct msgbuf*)&buffer, len, 0) < 0) {
                printf("Can't send message to queue\n");
                msgctl(msqid, IPC_RMID, NULL);
                exit(-1);
        }
    }

    printf("Program 1 was spawn %d times, program 2 - %d times, total - %d times", array[0], array[1], array[2]);

    if (shmdt(array) < 0) {
        printf("Can't detach memory\n");
        exit(-1);
    }

    return 0;
}
