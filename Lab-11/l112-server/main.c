#include <stdio.h>
#include <sys/msg.h>
#include <stdlib.h>

int main()
{
    int msqid;
    char pathname[] = "msgserv.m";
    int len, i = 0, maxlen = 81;
    key_t key;

    struct msgbuf {
        long mtype;
        struct {
            uint32_t sinfo;
            float finfo;
        } info;
    } buffer;

    if ((key = ftok(pathname, 0)) < 0) {
        printf("Can't generate key!\n");
        exit(-1);
    }

    if ((msqid = msgget(key, 0666 | IPC_CREAT)) < 0) {
        printf("Can't get msgqid!");
        exit(-1);
    }

    while(1) {
        // 2 - тип принимаемого сообщения, задается в buffer.mtype
        if ((len = msgrcv(msqid, (struct msgbuf*)&buffer, maxlen, 1, 0)) < 0) {
            printf("Can't receive message from queue\n");
            exit(-1);
        }

        printf("Client with PID %d connected!\n", buffer.info.sinfo);

        buffer.mtype = 3;

        if (msgsnd(msqid, (struct msgbuf*)&buffer, len, 0) < 0) {
            printf("Can't send message to queue\n");
            msgctl(msqid, IPC_RMID, NULL);
            exit(-1);
        }
    }

    return 0;
}
