#include <stdio.h>
#include <sys/msg.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ipc.h>

const short OK1 = 2;
const short OK2 = 3;

int main()
{
    int msqid;
    char pathname[] = "msgqueue.m";
    int len, i = 0;

    int semaphore = 0;

    key_t key;

    struct msgbuf {
        long mtype;
        struct {
            uint32_t sinfo;
            float finfo;
        } info;
    } buffer;

    buffer.mtype = 1; // Initial value

    if ((key = ftok(pathname, 0)) < 0) {
        printf("Can't generate key!\n");
        exit(-1);
    }

    if ((msqid = msgget(key, 0666 | IPC_CREAT)) < 0) {
        printf("Can't get msgqid!");
        exit(-1);
    }

        // 0 - тип принимаемого сообщения, задается в buffer.mtype
        if ((len = msgrcv(msqid, (struct msgbuf*)&buffer, sizeof(buffer), OK1, 0)) < 0) {
            printf("Can't receive message from queue\n");
            exit(-1);
        }

        buffer.mtype = OK2;

        if (msgsnd(msqid, (struct msgbuf*)&buffer, len, 0) < 0) {
            printf("Can't send message to queue\n");
            msgctl(msqid, IPC_RMID, NULL);
            exit(-1);
        }

    return 0;
}
