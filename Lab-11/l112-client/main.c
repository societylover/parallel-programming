#include <stdio.h>
#include <sys/msg.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
    int msqid;
    char pathname[] = "msgserv.m";
    int len, i = 0, maxlen = 81;
    key_t key;

    struct msgbuf {
        long mtype;
        struct {
            uint32_t sinfo;
            float finfo;
        } info;
    } buffer;

    if ((key = ftok(pathname, 0)) < 0) {
        printf("Can't generate key!\n");
        exit(-1);
    }

    if ((msqid = msgget(key, 0666 | IPC_CREAT)) < 0) {
        printf("Can't get msgqid!");
        exit(-1);
    }

    buffer.mtype = 1;
    buffer.info.sinfo = getpid();

    len = sizeof(buffer);

    if (msgsnd(msqid, (struct msgbuf*)&buffer, len, 0) < 0) {
        printf("Can't send message to queue\n");
        msgctl(msqid, IPC_RMID, NULL);
        exit(-1);
    }

    buffer.info.sinfo = 0;

    if ((len = msgrcv(msqid, (struct msgbuf*)&buffer, maxlen, 3, 0)) < 0) {
         printf("Can't receive message from queue\n");
         exit(-1);
    }

    if (buffer.info.sinfo == getpid()) printf("PID is same!\n");

    return 0;
}
