#include <stdio.h>
#include <sys/msg.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ipc.h>

const long LAST_MESSAGE = 255;

int main()
{
    int msqid;
    char pathname[] = "msgqueue.m";
    int i = 0, maxlen = 81;
    ssize_t len;

    key_t key;

    struct msgbuf {
        long mtype;
        char mtext[81];
    } buffer;

    buffer.mtype = 2; // Initial value

    if ((key = ftok(pathname, 0)) < 0) {
        printf("Can't generate key!\n");
        exit(-1);
    }

    if ((msqid = msgget(key, 0666 | IPC_CREAT)) < 0) {
        printf("Can't get msgqid!");
        exit(-1);
    }

    while (1) {

        // 1 - тип принимаемого сообщения, задается в buffer.mtype
        if ((len = msgrcv(msqid, (struct msgbuf*)&buffer, maxlen, 1, 0)) < 0) {
            printf("Can't receive message from queue\n");
            exit(-1);
        }

        printf("l111-b: Value type %ld, string received: %s\n", buffer.mtype, buffer.mtext);

        if (buffer.mtype == LAST_MESSAGE) {
            msgctl(msqid, IPC_RMID, (struct msqid_ds*)NULL);
            return 0;
        }

        if (i++ < 3) {
            buffer.mtype = 2;
            strcpy(buffer.mtext, "Value from l111-messages-b");
            len = strlen(buffer.mtext) + 1;
        } else {
            buffer.mtype = LAST_MESSAGE;
            len = 0;
        }

        if (msgsnd(msqid, (struct msgbuf*)&buffer, len, 0) < 0) {
            printf("Can't send message to queue\n");
            msgctl(msqid, IPC_RMID, NULL);
            exit(-1);
        }

        if (i >= 3) {
            msgctl(msqid, IPC_RMID, (struct msqid_ds*)NULL);
            return 0;
        }
    }

    return 0;
}
