cmake_minimum_required(VERSION 3.5)

project(l103-semaphore-pipe LANGUAGES C)

add_executable(l103-semaphore-pipe main.c)
