#include <stdio.h>
#include <sys/pipe.h>
#include <stdlib.h>
#include <sys/sem.h>
#include <unistd.h>

int main()
{
    int fd[2], result, semid;
    size_t size;
    char resstring[14];

    char *semaphore = "semaphore.s";

    key_t key;


    if((key = ftok(semaphore, 0)) < 0){

        printf("Can\'t generate key\n");
        exit(-1);

    }

    if (pipe(fd) < 0) {
        printf("Can't create pipe!\n");
        exit(-1);
    }

    result = fork();

    if (result < 0) {
        printf("Can't fork!\n");
        exit(-1);
    }

    struct sembuf mybuf;

    if ((semid = semget(key, 2, 0666 | IPC_CREAT)) < 0) {
        printf("Can't get semid!\n");
        exit(-1);
    }

    if (result > 0) {

        mybuf.sem_op = 1;
        mybuf.sem_flg = 0;
        mybuf.sem_num = 0;

        if (semop(semid, &mybuf, 1) < 0) {
            printf("Parent: can't wait condition\n");
            exit(-1);
        }

        size = write(fd[1], "Hello, world!", 14);

        if (size != 14) {
            printf("Parent: can't write full string!\n");
            exit(-1);
        }

        printf("Parent: write full string\n");

        mybuf.sem_op = -1;
        mybuf.sem_flg = 0;
        mybuf.sem_num = 1;

        if (semop(semid, &mybuf, 1) < 0) {
            printf("Parent: can't wait condition\n");
            exit(-1);
        }

        size = read(fd[0], resstring, 14);

        if (size < 0) {
            printf("Parent: can't read full string!\n");
            exit(-1);
        }

        printf("Parent: read string is %s\n", resstring);

        close(fd[0]);
        close(fd[1]);

    } else {

        mybuf.sem_op = -1;
        mybuf.sem_flg = 0;
        mybuf.sem_num = 0;

        if (semop(semid, &mybuf, 1) < 0) {
            printf("Parent: can't wait condition\n");
            exit(-1);
        }

        size = read(fd[0], resstring, 14);

        if (size < 0) {
            printf("Child: can't read full string!\n");
            exit(-1);
        }

        printf("Child: read string is %s\n", resstring);

        mybuf.sem_op = 1;
        mybuf.sem_flg = 0;
        mybuf.sem_num = 1;

        if (semop(semid, &mybuf, 1) < 0) {
            printf("Parent: can't wait condition\n");
            exit(-1);
        }

        size = write(fd[1], "Hello, moons!", 14);

        if (size != 14) {
            printf("Child: can't write full string!\n");
            exit(-1);
        }

        printf("Child: write full string\n");

        close(fd[0]);
        close(fd[1]);
    }

    return 0;
}
