cmake_minimum_required(VERSION 3.5)

project(l10-semaphore-test2 LANGUAGES C)

add_executable(l10-semaphore-test2 main.c)
