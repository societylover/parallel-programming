/* Программа 1 для иллюстрации некорректности работы с разделяемой памятью*/

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/sem.h>

int main()
{

    int *array;

    int shmid, semid;

    int new = 1;

    char pathname[] = "shared.s";

    key_t key; /* IPC ключ */

    if((key = ftok(pathname,0)) < 0){

        printf("Can\'t generate key\n");
        exit(-1);

    }

    if((shmid = shmget(key, 3*sizeof(int), 0666|IPC_CREAT|IPC_EXCL)) < 0){

        if(errno != EEXIST){


            /* Если по другой причине - прекращаем работу */

            printf("Can\'t create shared memory\n");
            exit(-1);

        } else {

            if((shmid = shmget(key, 3*sizeof(int), 0)) < 0){

                printf("Can\'t find shared memory\n");
                exit(-1);

            }
            new = 0;

        }

    }

    if((array = (int *)shmat(shmid, NULL, 0)) == (int *)(-1)){

        printf("Can't attach shared memory\n");
        exit(-1);

    }

    struct sembuf mybuf;

    if ((semid = semget(key, 1, 0666 | IPC_CREAT)) < 0) {
        printf("Can't get semid!");
        exit(-1);
    }

    if(new){

        array[0] = 1;
        array[1] = 0;
        array[2] = 1;

    } else {

        array[0] += 1;

        for(long i=0; i<1000000000L; i++);

        mybuf.sem_op = -1;
        mybuf.sem_flg = 0;
        mybuf.sem_num = 0;

       if (semop(semid, &mybuf, 1) < 0) {
            printf("Can't wait condition\n");
            exit(-1);
        }

        array[2] += 1;

    }

    printf("Program 1 was spawn %d times, program 2 - %d times, total - %d times\n",
           array[0], array[1], array[2]);

    printf("Condition is present\n");

    if(shmdt(array) < 0){
        printf("Can't detach shared memory\n");
        exit(-1);

    }
    return 0;

}
