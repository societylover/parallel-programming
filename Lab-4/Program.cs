﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Lab4
{
    class Program
    {
        static string buffer = new("");
        static bool bufferEmpty = true;
        static bool finish = false; 
        static List<string> createWritersList(int messagesCount, int messageLength)
        {
            List<string> result = new List<string>();
            var message = "";

            for (int i = 0; i < messageLength; i++) message += "1";
            for (int i = 0; i < messagesCount; i++) result.Add(message);

            return result;
        }

        // LOCK
        static void ReaderLock(object state)
        {
            List<string> messages = state as List<string>;

            while (!finish)
            {
                if (!bufferEmpty)
                {
                    lock ("read") 
                    { 
                        if (!bufferEmpty)
                        {
                            bufferEmpty = true;
                            messages.Add(buffer);
                        }    
                    } 
                }
            }
        }

        static void WriterLock(object state) 
        {
            List<string> messages = state as List<string>;
            int i = 0;
            while (i < messages.Count)
            {
                lock ("write")
                {
                    if (bufferEmpty)
                    {
                        buffer = messages[i++];
                        bufferEmpty = false;
                    }
                }
            }
        }

        static void ReadersAndWritersWithLock(int threadsCount, int messagesCount, int messageLength)
        {
            List<Thread> writers = new();
            List<Thread> readers = new();

            var writersList = createWritersList(messagesCount, messageLength);

            var readersList = new List<string>();

            for (int i = 0; i < threadsCount; i++)
            {
                writers.Add(new Thread(WriterLock));
                writers[i].Start(writersList);
                readers.Add(new Thread(ReaderLock));
                readers[i].Start(readersList);
            }

            for (int i = 0; i < writers.Count; i++) writers[i].Join();
            finish = true;
            for (int i = 0; i < readers.Count; i++) readers[i].Join();

            Console.WriteLine($"Было записано: { writersList.Count * writers.Count } записей");
            Console.WriteLine($"Было считано: { readersList.Count } записей"); 
        }

        // AutoResetEvent

        static List<string> myMessage = new ();
        static List<string> myMessageBf = new();

        static void ReaderAutoResetEvent(object state)
        { 
            var stateArray = state as object[];            
            var evFull = stateArray[0] as AutoResetEvent;
            var evEmpty = stateArray[1] as AutoResetEvent; 

            while (true)
            { 
                evFull.WaitOne(); 
                if (finish) return;
                myMessageBf.Add(buffer); 
                evEmpty.Set();     
            }
        }

        static void WriterAutoResetEvent(object state)
        {
            var stateArray = state as object[];
            var evFull = stateArray[0] as AutoResetEvent;
            var evEmpty = stateArray[1] as AutoResetEvent; 

            int i = 0;
            while (i < myMessage.Count)
            { 
                evEmpty.WaitOne(); 
                buffer = myMessage[i++]; 
                evFull.Set();
            }

        }  

        static void ReadersAndWritersWithAutoResetEvent(int threadsCount, int messagesCount, int messageLength)
        {
            List<Thread> writers = new();
            List<Thread> readers = new();

            myMessage = createWritersList(messagesCount, messageLength);  

            AutoResetEvent evEmpty = new AutoResetEvent(false);
            AutoResetEvent evFull = new AutoResetEvent(false);

            for (int i = 0; i < threadsCount; i++)
            { 
                writers.Add(new Thread(WriterAutoResetEvent));
                writers[i].Start(new object[] { evFull , evEmpty });
                readers.Add(new Thread(ReaderAutoResetEvent));
                readers[i].Start(new object[] { evFull , evEmpty });
            }

            evEmpty.Set();
            for (int i = 0; i < writers.Count; i++) writers[i].Join(); 
            finish = true;
            Console.WriteLine($"Было записано: { myMessage.Count * threadsCount } записей");
            Console.WriteLine($"Было считано: { myMessageBf.Count } записей");
        }

        // Semaphore
        static void ReaderSemaphore(object state)
        {
            var semReader = state as SemaphoreSlim;

            while (!finish)
            { 
                if (!bufferEmpty)
                { 
                    semReader.Wait(); 
                    if (!bufferEmpty)
                    {
                        bufferEmpty = true;
                        myMessageBf.Add(buffer);
                    }
                    semReader.Release();
                } 
            }
        }

        static void WriterSemaphore(object state)
        {
            var semWriter = state as SemaphoreSlim;

            int i = 0;
            while (i < myMessage.Count)
            { 
                if (bufferEmpty)
                { 
                    semWriter.Wait(); 
                    if (bufferEmpty)
                    {
                        bufferEmpty = false;
                        
                        buffer = myMessage[i++];
                    }
                }
                semWriter.Release(); 
            }
        }

        static void ReadersAndWritersWithSemaphore(int threadsCount, int messagesCount, int messageLength)
        {
            List<Thread> writers = new();
            List<Thread> readers = new();

            myMessage = createWritersList(messagesCount, messageLength);

            var readersList = new List<string>();

            bufferEmpty = true;

            SemaphoreSlim semaphoreSlim = new SemaphoreSlim(threadsCount);

            for (int i = 0; i < threadsCount; i++)
            {
                writers.Add(new Thread(WriterSemaphore));
                writers[i].Start(semaphoreSlim);
                readers.Add(new Thread(ReaderSemaphore));
                readers[i].Start(semaphoreSlim);
            }

            for (int i = 0; i < writers.Count; i++) writers[i].Join();
            finish = true;
            for (int i = 0; i < readers.Count; i++) readers[i].Join();

            Console.WriteLine($"Было записано: { myMessage.Count * writers.Count } записей");
            Console.WriteLine($"Было считано: { myMessageBf.Count } записей");
        }

        // Interlocked

        // 0 no, 1 yes
        static int bufferEmptyInterlocked = 1;
        static long bufferEmptyInterlocked2 = 1;

        static void ReaderInterlocked()
        {
            while (!finish)
            {
                if (0 == Interlocked.Exchange(ref bufferEmptyInterlocked2, 1))
                {  
                        myMessageBf.Add(buffer);
                        Interlocked.Exchange(ref bufferEmptyInterlocked, 1); 
                }
            }
        }

        static void WriterInterlocked()
        {
            int i = 0;
            while (i < myMessage.Count)
            {
                if (1 == Interlocked.Exchange(ref bufferEmptyInterlocked, 0))
                {  
                        buffer = myMessage[i++];
                        Interlocked.Exchange(ref bufferEmptyInterlocked2, 0); 
                }
            }
        }

        static void ReadersAndWritersWithInterlocked(int threadsCount, int messagesCount, int messageLength)
        {
            List<Thread> writers = new();
            List<Thread> readers = new();

            myMessage = createWritersList(messagesCount, messageLength);
            
            for (int i = 0; i < threadsCount; i++)
            {
                writers.Add(new Thread(WriterInterlocked));
                writers[i].Start();
                readers.Add(new Thread(ReaderInterlocked));
                readers[i].Start();
            }

            for (int i = 0; i < writers.Count; i++) writers[i].Join();
            finish = true;
            for (int i = 0; i < readers.Count; i++) readers[i].Join();

            Console.WriteLine($"Было записано: { myMessage.Count * writers.Count } записей");
            Console.WriteLine($"Было считано: { myMessageBf.Count } записей");
        }

        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            // ReadersAndWritersWithLock(10, 100000, 100);
            // ReadersAndWritersWithAutoResetEvent(10, 100000, 100);
            // ReadersAndWritersWithSemaphore(10, 100000, 100);
            ReadersAndWritersWithInterlocked(2, 100000, 100);
            sw.Stop();
            Console.WriteLine(sw.Elapsed.TotalMilliseconds);  
        }
    }
}
