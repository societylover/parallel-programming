﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Lab3 
{
    class Program
    {

        static List<int> createBasePrimeNumbersList(int N)
        {
            int sqrtN = (int)Math.Sqrt(N);
            var result = Enumerable.Range(2, sqrtN).ToList(); 

            for (int i = 0; i < result.Count; i++) 
                for (int j = i + result[i]; j < result.Count; result.RemoveAt(j++));
             
            return result;
        }

        // No thread prime numbers list
        static List<int> getPrimeNumbersFromNItems(List<int> basicPrimes, int numberStart, int numberEnd, bool createNew)
        {
            List<int> result;
            if (createNew) result = new List<int>();
            else result = new List<int>(basicPrimes); 

            for (int i = numberStart; i < numberEnd; i++)
            {
                bool badNumber = false;
                for (int j = 0; j < basicPrimes.Count; j++)
                    if (i % basicPrimes[j] == 0) { 
                        badNumber = true; 
                        break; 
                    }
                if (!badNumber) result.Add(i);
            }

            return result;
        }


        // #1 Data Decompose

        static List<List<int>> primeNumbersThDataDecompose = new List<List<int>>(); 

        static void primeNumbersWDataDecomposeThread(object state)
        {
            object[] stateArray = state as object[]; 
            var basicPrimes = stateArray[0] as List<int>;
            int numberStart = (int)stateArray[1];
            int numberEnd = (int)stateArray[2];
            int threadResultIndex = (int)stateArray[3]; 
            var result = getPrimeNumbersFromNItems(basicPrimes, numberStart, numberEnd, true);
            primeNumbersThDataDecompose[threadResultIndex] = result;
        }

        static List<int> getPrimeNumbersFromNItemsDataDecompose(int N, int ThreadCount = 1)
        {
            primeNumbersThDataDecompose = Enumerable.Repeat(new List<int>(), ThreadCount).ToList();
            List<int> basics = createBasePrimeNumbersList(N); 
            List<Thread> threads = new List<Thread>(ThreadCount);

            int threadStepNumber = (N - basics.Last())/ ThreadCount;
            int startIndexedNumber = basics.Last();

            for (int i = 0; i < ThreadCount; i++)
            {
                threads.Add(new Thread(primeNumbersWDataDecomposeThread)); 
                threads[i].Start(new object[] { basics, startIndexedNumber, startIndexedNumber + threadStepNumber, i });
                startIndexedNumber += threadStepNumber + 1;
            }
 
            threads.ForEach(it => it.Join()); 

            List<int> result = new List<int>();
            basics.ForEach(it => result.Add(it));
            primeNumbersThDataDecompose.ForEach(it => it.ForEach(value => result.Add(value))); 
            return result;
        }

        // #2 Prime Numbers Decompose

        static List<List<int>> primeNumbersThNumbersDecompose = new List<List<int>>();

        static void primeNumbersWPrimeNumberDecomposeThread(object state)
        {
            object[] stateArray = state as object[];
            int primeNumberStart = (int)stateArray[1];
            int primeCount = (int)stateArray[2];
            var basicPrimes = (stateArray[0] as List<int>).GetRange(primeNumberStart, primeCount);
            int threadResultIndex = (int)stateArray[3];
            int N = (int)stateArray[4];   
            var result = getPrimeNumbersFromNItems(basicPrimes, (stateArray[0] as List<int>).Last() + 1, N, true);
            primeNumbersThNumbersDecompose[threadResultIndex] = result;
        }

        static List<int> getPrimeNumbersFromNItemsPrimeNumbersDecompose(int N, int ThreadCount = 1)
        {
            primeNumbersThNumbersDecompose = Enumerable.Repeat(new List<int>(), ThreadCount).ToList();
            List<int> basics = createBasePrimeNumbersList(N);
            List<Thread> threads = new List<Thread>(ThreadCount);

            int basicCountForThread = basics.Count / ThreadCount; 
            int startIndexedNumber = 0; 
            for (int i = 0; i < ThreadCount; i++)
            {
                threads.Add(new Thread(primeNumbersWPrimeNumberDecomposeThread)); 
                threads[i].Start(new object[] { basics, startIndexedNumber, basicCountForThread, i, N }); 
                startIndexedNumber += basicCountForThread;
            }

            threads.ForEach(it => it.Join());

            List<int> unsortedRes = new List<int>(); 
            primeNumbersThNumbersDecompose.ForEach(it => it.ForEach(value => unsortedRes.Add(value))); 
            unsortedRes = unsortedRes.Where((it) => unsortedRes.Count((value) => value == it) > ThreadCount-1).ToList();
            
            (unsortedRes = unsortedRes.Distinct().ToList()).Sort();
            unsortedRes.InsertRange(0, basics);

            return unsortedRes;
        }

        // #3 Prime Numbers Brute Force

        static List<int> basePrimeNumberList;
        static List<List<int>> primeNumbersThBruteForce = new List<List<int>>();

        static void primeNumbersWPrimeNumbersBruteForce(object state)
        {
            var objectArray = state as object[]; 
            var firstPrimeNumberIndex = (int)objectArray[0];
            var threadCount = (int)objectArray[1];
            var N = (int)objectArray[2]; 

            for (int i = firstPrimeNumberIndex; i < basePrimeNumberList.Count; i += threadCount)
            {
                var currentPrimeListFrom1Element = new List<int>() { basePrimeNumberList[i] }; 
                primeNumbersThBruteForce[i] = getPrimeNumbersFromNItems(currentPrimeListFrom1Element, basePrimeNumberList.Last() + 1, N, true); 
            }   
        }

        static List<int> getPrimeNumbersFromNItemsPrimeNumbersBruteForce(int N, int ThreadCount = 1)
        {
            basePrimeNumberList = createBasePrimeNumbersList(N);
            primeNumbersThBruteForce = Enumerable.Repeat(new List<int>(), basePrimeNumberList.Count).ToList();
            List<Thread> threads = new List<Thread>(ThreadCount);
             
            for (int i = 0; i < ThreadCount; i++)
            {
                threads.Add(new Thread(primeNumbersWPrimeNumbersBruteForce));
                threads[i].Start(new object[] { i, ThreadCount, N }); 
            }

            threads.ForEach(it => it.Join());

            List<int> unsortedRes = new List<int>();
            primeNumbersThBruteForce.ForEach(it => it.ForEach(value => unsortedRes.Add(value)));
            unsortedRes = unsortedRes.Where((it) => unsortedRes.Count((value) => value == it) == basePrimeNumberList.Count).ToList();

            (unsortedRes = unsortedRes.Distinct().ToList()).Sort();
            unsortedRes.InsertRange(0, basePrimeNumberList);

            return unsortedRes;
        }

        static void Main(string[] args)
        {
            var basics = createBasePrimeNumbersList(100);

            getPrimeNumbersFromNItems(basics, basics.Last() + 1, 100, false).ForEach(it=>Console.WriteLine(it));
            getPrimeNumbersFromNItemsDataDecompose(100, 5).ForEach(it => Console.WriteLine(it)); 
            getPrimeNumbersFromNItemsPrimeNumbersDecompose(100, 5).ForEach(it => Console.WriteLine(it));
            getPrimeNumbersFromNItemsPrimeNumbersBruteForce(100,2).ForEach(it => Console.WriteLine(it));
        }
    }
}
