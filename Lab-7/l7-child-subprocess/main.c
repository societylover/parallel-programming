#include <stdio.h>
#include <unistd.h>

int main()
{
    printf("\nChild proccess subprogram!\n");
    printf("Parent PID is: %d", getppid());
    return 0;
}
