#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int calculateFactorial(int N) {
    int result = N--;
    while (N > 1) {
        result *= N--;
    }
    return result;
}

int calculateThirdPow(int N) {
    return N * N * N;
}

int main(int argc, char *argv[], char *envp[])
{
    // 1
    printf("1. current pid/ppid\n");
    printf("Current PID is: %d\n", getpid());
    printf("Current PPID is: %d\n", getppid());
    // 2
    printf("2. fork example\n");
    int pid = fork();
    if (pid == -1) {
        printf("Fork error!");
    } else if (pid == 0) {
        printf("\nChild fork\n");
        printf("Factorial result(5) = %d\n\n", calculateFactorial(5));
    } else {
        printf ("Parent fork\n");
        printf("Third Pow result(5) = %d\n\n", calculateThirdPow(5));
    }

    // 3
    printf("3. print argv and envp\n");
    for (int i = 1; i < argc; i++) printf(" %s", argv[i]);
    for (char **i = envp; *i != 0; i++) printf(" %s", *i);
    printf("\n");

    // 4
    if (pid == 0) {
        printf("\n4. Child fork \n");
        char * argv_new[2];
        argv_new[0] = "l7-child-subprocess";
        argv_new[1] = 0;
        execv("Documents/GitHub/ASTU/parallel-programming/l7-parallel-pid/l7-child-subprocess/l7-child-subprocess", argv_new);
    }

    return 0;
}
