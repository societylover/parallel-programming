﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Lab_1
{
    class Program
    {
        static List<TimeSpan> times = new List<TimeSpan>();
        // Для динамического получения имени потока
        static string[] threadNames = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "A2", "B2", "C2", "D2", "E2", "F2" };
        // Для вставки значения затраченного времени в список
        static void add(TimeSpan time)
        {
            lock (times)
            {
                times.Add(time);
            }
        }

        static void Main(string[] args)
        {
            List<Thread> threads = new List<Thread>();
            // Меням значение для каждого теста
            uint CORES_COUNT = 19;

            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Lowest }); // A
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Highest }); // B
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Lowest }); // C
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Highest }); // D
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Lowest }); // E
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Lowest }); // F
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Highest }); // G
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Highest }); // H
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Highest }); // I
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Highest }); // J
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Highest }); // K
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Highest }); // L
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Lowest }); // M
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Lowest }); // A2
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Lowest }); // B2
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Lowest }); // C2
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Lowest }); // D2
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Lowest }); // E2
            threads.Add(new Thread(threadFunction) { Priority = ThreadPriority.Highest }); // F2 

            // Поочередный запуск потоков для выполнения заданий
            for (int i = 0; i < CORES_COUNT; i++) threads[i].Start(threadNames[i]);

            // Остановка основного потока на 30 секунд, остальные потоки должны успеть за это время
            Thread.CurrentThread.Join(30000);

            // Время выполнения - время, затраченное самым "медленным" потоком
            Console.WriteLine(times.Max());
        }

        // Статическая функция 
        public static void threadFunction(object name)
        {
            DateTime t1 = DateTime.Now;
            double x = 12345.6789;
            for (int i = 0; i < 10000; i++)
                for (int j = 0; j < 10000; j++)
                {
                    x = Math.Sqrt(x);
                    x = x + 0.000000001;
                    x = Math.Pow(x, 2);
                }
            Console.WriteLine($"Thread {name} finished");
            // Запишем результат очередного потока 
            add(DateTime.Now - t1);
        }

    }
}

