﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq; 
using System.Threading;
using System.Threading.Tasks;

namespace Lab_2
{
    class Program
    {
        static TimeSpan timeElapsedEasy(int N)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            List<double> data = new List<double>();

            for (int j = 0; j < N; data.Add(2 * j++)) { }
            for (int j = 0; j < N; j++) { data[j] = Math.Sqrt(Math.Pow(data[j], 2.34)); }

            sw.Stop();
            return sw.Elapsed;
        }
        static TimeSpan timeElapsedHard(int N)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            List<double> data = new List<double>();

            for (int j = 0; j < N; data.Add(2*j++)) { }
            for (int j = 0; j < N; data[j] = 
                Math.Pow(data[j], 
                Math.Pow(data[j], 
                Math.Pow(data[j], 
                Math.Sqrt(Math.Pow(data[j], 2.34))))), j++) { } 

            sw.Stop();
            return sw.Elapsed;
        }

        public static void threadFunctionEasy(object state)
        {
            object[] array = state as object[];
            List<double> data = array[1] as List<double>;
            for (int i = (int)array[2]; i < (int)array[3]; i++)
            {
                data[i] =  Math.Sqrt(Math.Pow(data[i], 2.34)); 
            } 

            // Сигнализируем о завершении обработки
            ManualResetEvent ev = array[0] as ManualResetEvent;
            ev.Set(); 
        }

        public static void threadFunctionHard(object state)
        {
            object[] array = state as object[];
            List<double> data = array[1] as List<double>;
            for (int i = (int)array[2]; i < (int)array[3]; i++)
            {
                data[i] =
                Math.Pow(data[i],
                Math.Pow(data[i],
                Math.Pow(data[i],
                Math.Sqrt(Math.Pow(data[i], 2.34)))));
            }


            // Сигнализируем о завершении обработки
            ManualResetEvent ev = array[0] as ManualResetEvent;
            ev.Set();
        }

        public static void threadFunctionHardCycle(object state)
        {
            object[] array = state as object[];
            List<double> data = array[1] as List<double>;
            for (int i = (int)array[2]; i < data.Count; i += (ushort)array[3])
            {
                
                data[i] =
                Math.Pow(data[i],
                Math.Pow(data[i],
                Math.Pow(data[i],
                Math.Sqrt(Math.Pow(data[i], 2.34))))); 
            }

            // Сигнализируем о завершении обработки
            ManualResetEvent ev = array[0] as ManualResetEvent;
            ev.Set();
        }


        static TimeSpan timeElapsedHard(int N, ushort M)
        {
            ManualResetEvent[] events = new ManualResetEvent[M]; 
            Stopwatch sw = new Stopwatch(); 
            sw.Start();

            List<double> data = new List<double>(N);
            for (int j = 0; j < N; data.Add(2 * j++)) { }  

            for (int i = 0, part = N / M; i < M; i++)
            { 
                Thread th = new Thread(threadFunctionHard);
                events[i] = new ManualResetEvent(false); 
                th.Start(new object[] { events[i], data, i * part, i * part + part }); 
            }

            WaitHandle.WaitAll(events);

            sw.Stop();
            return sw.Elapsed;
        }

        static TimeSpan timeElapsedHardCycle(int N, ushort M)
        {
            ManualResetEvent[] events = new ManualResetEvent[M];
            Stopwatch sw = new Stopwatch();
            sw.Start();

            List<double> data = new List<double>(N);
            for (int j = 0; j < N; data.Add(2 * j++)) { }

            for (int i = 0; i < M; i++)
            {
                Thread th = new Thread(threadFunctionHardCycle);
                events[i] = new ManualResetEvent(false);
                th.Start(new object[] { events[i], data, i, M });
            }

            WaitHandle.WaitAll(events);

            sw.Stop();
            return sw.Elapsed;
        }

        static TimeSpan timeElapsedEasy(int N, UInt16 M)
        {
            ManualResetEvent[] events = new ManualResetEvent[M];
            Stopwatch sw = new Stopwatch();
            sw.Start();

            List<double> data = new List<double>(N);
            for (int j = 0; j < N; data.Add(2 * j++)) { }

            for (int i = 0, part = N / M; i < M; i++)
            {
                Thread th = new Thread(threadFunctionEasy);
                events[i] = new ManualResetEvent(false);
                th.Start(new object[] { events[i], data, i * part, i * part + part });
            }

            WaitHandle.WaitAll(events);

            sw.Stop();
            return sw.Elapsed;
        }

        static void Main(string[] args)
        {
            //Console.WriteLine("10-2Easy: Total time: {0}", timeElapsedEasy(10, 2).TotalMilliseconds);
            //Console.WriteLine("100-2Easy: Total time: {0}", timeElapsedEasy(100, 2).TotalMilliseconds);
            //Console.WriteLine("1000-2Easy:Total time: {0}", timeElapsedEasy(1000, 2).TotalMilliseconds);
            //Console.WriteLine("100000-2Easy:Total time: {0}", timeElapsedEasy(100000, 2).TotalMilliseconds);
            //Console.WriteLine("1000000-2Easy:Total time: {0}", timeElapsedEasy(1000000, 2).TotalMilliseconds);
            //Console.WriteLine("10000000-2Easy:Total time: {0}", timeElapsedEasy(10000000, 2).TotalMilliseconds);

            //Console.WriteLine("10-2Hard: Total time: {0}", timeElapsedHard(10, 2).TotalMilliseconds);
            //Console.WriteLine("100-2Hard: Total time: {0}", timeElapsedHard(100, 2).TotalMilliseconds);
            //Console.WriteLine("1000-2Hard: Total time: {0}", timeElapsedHard(1000, 2).TotalMilliseconds);
            //Console.WriteLine("100000-2Hard: Total time: {0}", timeElapsedHard(100000, 2).TotalMilliseconds);
            //Console.WriteLine("1000000-2Hard: Total time: {0}", timeElapsedHard(1000000, 2).TotalMilliseconds);
            //Console.WriteLine("10000000-2Hard: Total time: {0}", timeElapsedHard(10000000, 2).TotalMilliseconds);

            //Console.WriteLine("10-3Easy: Total time: {0}", timeElapsedEasy(10, 3).TotalMilliseconds);
            //Console.WriteLine("100-3Easy: Total time: {0}", timeElapsedEasy(100, 3).TotalMilliseconds);
            //Console.WriteLine("1000-3Easy:Total time: {0}", timeElapsedEasy(1000, 3).TotalMilliseconds);
            //Console.WriteLine("100000-3Easy:Total time: {0}", timeElapsedEasy(100000, 3).TotalMilliseconds);
            //Console.WriteLine("1000000-3Easy:Total time: {0}", timeElapsedEasy(1000000, 3).TotalMilliseconds);
            //Console.WriteLine("10000000-3Easy:Total time: {0}", timeElapsedEasy(10000000, 3).TotalMilliseconds);

            //Console.WriteLine("10-3Hard: Total time: {0}", timeElapsedHard(10, 3).TotalMilliseconds);
            //Console.WriteLine("100-3Hard: Total time: {0}", timeElapsedHard(100, 3).TotalMilliseconds);
            //Console.WriteLine("1000-3Hard: Total time: {0}", timeElapsedHard(1000, 3).TotalMilliseconds);
            //Console.WriteLine("100000-3Hard: Total time: {0}", timeElapsedHard(100000, 3).TotalMilliseconds);
            //Console.WriteLine("1000000-3Hard: Total time: {0}", timeElapsedHard(1000000, 3).TotalMilliseconds);
            //Console.WriteLine("10000000-3Hard: Total time: {0}", timeElapsedHard(10000000, 3).TotalMilliseconds);

            //Console.WriteLine("10-4Easy: Total time: {0}", timeElapsedEasy(10, 4).TotalMilliseconds);
            //Console.WriteLine("100-4Easy: Total time: {0}", timeElapsedEasy(100, 4).TotalMilliseconds);
            //Console.WriteLine("1000-4Easy:Total time: {0}", timeElapsedEasy(1000, 4).TotalMilliseconds);
            //Console.WriteLine("100000-4Easy:Total time: {0}", timeElapsedEasy(100000, 4).TotalMilliseconds);
            //Console.WriteLine("1000000-4Easy:Total time: {0}", timeElapsedEasy(1000000, 4).TotalMilliseconds);
            //Console.WriteLine("10000000-4Easy:Total time: {0}", timeElapsedEasy(10000000, 4).TotalMilliseconds);

            //Console.WriteLine("10-4Hard: Total time: {0}", timeElapsedHard(10, 4).TotalMilliseconds);
            //Console.WriteLine("100-4Hard: Total time: {0}", timeElapsedHard(100, 4).TotalMilliseconds);
            //Console.WriteLine("1000-4Hard: Total time: {0}", timeElapsedHard(1000, 4).TotalMilliseconds);
            //Console.WriteLine("100000-4Hard: Total time: {0}", timeElapsedHard(100000, 4).TotalMilliseconds);
            //Console.WriteLine("1000000-4Hard: Total time: {0}", timeElapsedHard(1000000, 4).TotalMilliseconds);
            //Console.WriteLine("10000000-4Hard: Total time: {0}", timeElapsedHard(10000000, 4).TotalMilliseconds);

            //Console.WriteLine("10-5Easy: Total time: {0}", timeElapsedEasy(10, 5).TotalMilliseconds);
            //Console.WriteLine("100-5Easy: Total time: {0}", timeElapsedEasy(100, 5).TotalMilliseconds);
            //Console.WriteLine("1000-5Easy:Total time: {0}", timeElapsedEasy(1000, 5).TotalMilliseconds);
            //Console.WriteLine("100000-5Easy:Total time: {0}", timeElapsedEasy(100000, 5).TotalMilliseconds);
            //Console.WriteLine("1000000-5Easy:Total time: {0}", timeElapsedEasy(1000000, 5).TotalMilliseconds);
            //Console.WriteLine("10000000-5Easy:Total time: {0}", timeElapsedEasy(10000000, 5).TotalMilliseconds);

            //Console.WriteLine("10-5Hard: Total time: {0}", timeElapsedHard(10, 5).TotalMilliseconds);
            //Console.WriteLine("100-5Hard: Total time: {0}", timeElapsedHard(100, 5).TotalMilliseconds);
            //Console.WriteLine("1000-5Hard: Total time: {0}", timeElapsedHard(1000, 5).TotalMilliseconds);
            //Console.WriteLine("100000-5Hard: Total time: {0}", timeElapsedHard(100000, 5).TotalMilliseconds);
            //Console.WriteLine("1000000-5Hard: Total time: {0}", timeElapsedHard(1000000, 5).TotalMilliseconds);
            //Console.WriteLine("10000000-5Hard: Total time: {0}", timeElapsedHard(10000000, 5).TotalMilliseconds);

            //Console.WriteLine("10-10Easy: Total time: {0}", timeElapsedEasy(10, 10).TotalMilliseconds);
            //Console.WriteLine("100-10Easy: Total time: {0}", timeElapsedEasy(100, 10).TotalMilliseconds);
            // Console.WriteLine("1000-10Easy:Total time: {0}", timeElapsedEasy(1000, 10).TotalMilliseconds);
            //Console.WriteLine("100000-10Easy:Total time: {0}", timeElapsedEasy(100000, 10).TotalMilliseconds);
            //Console.WriteLine("1000000-10Easy:Total time: {0}", timeElapsedEasy(1000000, 10).TotalMilliseconds);
            //Console.WriteLine("10000000-10Easy:Total time: {0}", timeElapsedEasy(10000000, 10).TotalMilliseconds);

            //Console.WriteLine("10-10Hard: Total time: {0}", timeElapsedHard(10, 10).TotalMilliseconds);
            //Console.WriteLine("100-10Hard: Total time: {0}", timeElapsedHard(100, 10).TotalMilliseconds);
            //Console.WriteLine("1000-10Hard: Total time: {0}", timeElapsedHard(1000, 10).TotalMilliseconds);
            //Console.WriteLine("100000-10Hard: Total time: {0}", timeElapsedHard(100000, 10).TotalMilliseconds);
            //Console.WriteLine("1000000-10Hard: Total time: {0}", timeElapsedHard(1000000, 10).TotalMilliseconds);
            //Console.WriteLine("10000000-10Hard: Total time: {0}", timeElapsedHard(10000000, 10).TotalMilliseconds);

            //Console.WriteLine("10000000-10Hard: Total time: {0}", timeElapsedHard(10, 2).TotalMilliseconds);
            //Console.WriteLine("10000000-10Hard: Total time: {0}", timeElapsedHardCycle(10, 1).TotalMilliseconds);

            //Console.WriteLine("10-2Hard: Total time: {0}", timeElapsedHard(10, 2).TotalMilliseconds);
            //Console.WriteLine("100-2Hard: Total time: {0}", timeElapsedHard(100, 2).TotalMilliseconds);
            //Console.WriteLine("1000-2Hard: Total time: {0}", timeElapsedHard(1000, 2).TotalMilliseconds);
            //Console.WriteLine("100000-2Hard: Total time: {0}", timeElapsedHard(100000, 2).TotalMilliseconds);
            //Console.WriteLine("1000000-2Hard: Total time: {0}", timeElapsedHard(1000000, 2).TotalMilliseconds);
            //Console.WriteLine("10000000-2Hard: Total time: {0}", timeElapsedHard(10000000, 2).TotalMilliseconds);

            //Console.WriteLine("10-2HardCycle: Total time: {0}", timeElapsedHardCycle(10, 2).TotalMilliseconds);
            //Console.WriteLine("100-2HardCycle: Total time: {0}", timeElapsedHardCycle(100, 2).TotalMilliseconds);
            //Console.WriteLine("1000-2HardCycle: Total time: {0}", timeElapsedHardCycle(1000, 2).TotalMilliseconds);
            //Console.WriteLine("100000-2HardCycle: Total time: {0}", timeElapsedHardCycle(100000, 2).TotalMilliseconds);
            //Console.WriteLine("1000000-2HardCycle: Total time: {0}", timeElapsedHardCycle(1000000, 2).TotalMilliseconds);
            //Console.WriteLine("10000000-2HardCycle: Total time: {0}", timeElapsedHardCycle(10000000, 2).TotalMilliseconds);

            //Console.WriteLine("10-3Hard: Total time: {0}", timeElapsedHard(10, 3).TotalMilliseconds);
            //Console.WriteLine("100-3Hard: Total time: {0}", timeElapsedHard(100, 3).TotalMilliseconds);
            //Console.WriteLine("1000-3Hard: Total time: {0}", timeElapsedHard(1000, 3).TotalMilliseconds);
            //Console.WriteLine("100000-3Hard: Total time: {0}", timeElapsedHard(100000, 3).TotalMilliseconds);
            //Console.WriteLine("1000000-3Hard: Total time: {0}", timeElapsedHard(1000000, 3).TotalMilliseconds);
            //Console.WriteLine("10000000-3Hard: Total time: {0}", timeElapsedHard(10000000, 3).TotalMilliseconds);

            //Console.WriteLine("10-3HardCycle: Total time: {0}", timeElapsedHardCycle(10, 3).TotalMilliseconds);
            //Console.WriteLine("100-3HardCycle: Total time: {0}", timeElapsedHardCycle(100, 3).TotalMilliseconds);
            //Console.WriteLine("1000-3HardCycle: Total time: {0}", timeElapsedHardCycle(1000, 3).TotalMilliseconds);
            //Console.WriteLine("100000-3HardCycle: Total time: {0}", timeElapsedHardCycle(100000, 3).TotalMilliseconds);
            //Console.WriteLine("1000000-3HardCycle: Total time: {0}", timeElapsedHardCycle(1000000, 3).TotalMilliseconds);
            //Console.WriteLine("10000000-3HardCycle: Total time: {0}", timeElapsedHardCycle(10000000, 3).TotalMilliseconds);

            //Console.WriteLine("10-4Hard: Total time: {0}", timeElapsedHard(10, 4).TotalMilliseconds);
            //Console.WriteLine("100-4Hard: Total time: {0}", timeElapsedHard(100, 4).TotalMilliseconds);
            //Console.WriteLine("1000-4Hard: Total time: {0}", timeElapsedHard(1000, 4).TotalMilliseconds);
            //Console.WriteLine("100000-4Hard: Total time: {0}", timeElapsedHard(100000, 4).TotalMilliseconds);
            //Console.WriteLine("1000000-4Hard: Total time: {0}", timeElapsedHard(1000000, 4).TotalMilliseconds);
            //Console.WriteLine("10000000-4Hard: Total time: {0}", timeElapsedHard(10000000, 4).TotalMilliseconds);

            //Console.WriteLine("10-4HardCycle: Total time: {0}", timeElapsedHardCycle(10, 4).TotalMilliseconds);
            //Console.WriteLine("100-4HardCycle: Total time: {0}", timeElapsedHardCycle(100, 4).TotalMilliseconds);
            //Console.WriteLine("1000-4HardCycle: Total time: {0}", timeElapsedHardCycle(1000, 4).TotalMilliseconds);
            //Console.WriteLine("100000-4HardCycle: Total time: {0}", timeElapsedHardCycle(100000, 4).TotalMilliseconds);
            //Console.WriteLine("1000000-4HardCycle: Total time: {0}", timeElapsedHardCycle(1000000, 4).TotalMilliseconds);
            //Console.WriteLine("10000000-4HardCycle: Total time: {0}", timeElapsedHardCycle(10000000, 4).TotalMilliseconds);

            //Console.WriteLine("10-5Hard: Total time: {0}", timeElapsedHard(10, 5).TotalMilliseconds);
            //Console.WriteLine("100-5Hard: Total time: {0}", timeElapsedHard(100, 5).TotalMilliseconds);
            //Console.WriteLine("1000-5Hard: Total time: {0}", timeElapsedHard(1000, 5).TotalMilliseconds);
            //Console.WriteLine("100000-5Hard: Total time: {0}", timeElapsedHard(100000, 5).TotalMilliseconds);
            //Console.WriteLine("1000000-5Hard: Total time: {0}", timeElapsedHard(1000000, 5).TotalMilliseconds);
            //Console.WriteLine("10000000-5Hard: Total time: {0}", timeElapsedHard(10000000, 5).TotalMilliseconds);

            //Console.WriteLine("10-5HardCycle: Total time: {0}", timeElapsedHardCycle(10, 5).TotalMilliseconds);
            //Console.WriteLine("100-5HardCycle: Total time: {0}", timeElapsedHardCycle(100, 5).TotalMilliseconds);
            //Console.WriteLine("1000-5HardCycle: Total time: {0}", timeElapsedHardCycle(1000, 5).TotalMilliseconds);
            //Console.WriteLine("100000-5HardCycle: Total time: {0}", timeElapsedHardCycle(100000, 5).TotalMilliseconds);
            //Console.WriteLine("1000000-5HardCycle: Total time: {0}", timeElapsedHardCycle(1000000, 5).TotalMilliseconds);
            //Console.WriteLine("10000000-5HardCycle: Total time: {0}", timeElapsedHardCycle(10000000, 5).TotalMilliseconds);

            //Console.WriteLine("10-10Hard: Total time: {0}", timeElapsedHard(10, 10).TotalMilliseconds);
            //Console.WriteLine("100-10Hard: Total time: {0}", timeElapsedHard(100, 10).TotalMilliseconds);
            //Console.WriteLine("1000-10Hard: Total time: {0}", timeElapsedHard(1000, 10).TotalMilliseconds);
            //Console.WriteLine("100000-10Hard: Total time: {0}", timeElapsedHard(100000, 10).TotalMilliseconds);
            //Console.WriteLine("1000000-10Hard: Total time: {0}", timeElapsedHard(1000000, 10).TotalMilliseconds);
            //Console.WriteLine("10000000-10Hard: Total time: {0}", timeElapsedHard(10000000, 10).TotalMilliseconds);

            //Console.WriteLine("10-10HardCycle: Total time: {0}", timeElapsedHardCycle(10, 10).TotalMilliseconds);
            //Console.WriteLine("100-10HardCycle: Total time: {0}", timeElapsedHardCycle(100, 10).TotalMilliseconds);
            //Console.WriteLine("1000-10HardCycle: Total time: {0}", timeElapsedHardCycle(1000, 10).TotalMilliseconds);
            //Console.WriteLine("100000-10HardCycle: Total time: {0}", timeElapsedHardCycle(100000, 10).TotalMilliseconds);
            //Console.WriteLine("1000000-10HardCycle: Total time: {0}", timeElapsedHardCycle(1000000, 10).TotalMilliseconds);
            //Console.WriteLine("10000000-10HardCycle: Total time: {0}", timeElapsedHardCycle(10000000, 10).TotalMilliseconds);
        }
    }
}
