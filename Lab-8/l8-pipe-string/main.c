#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>


int main()
{
    int fd[2];
    size_t size;
    char string[] = "Hello, world!";
    char resstring[14];

    if (pipe(fd) < 0) {
        printf("Can't create pipe");
        exit (-1);
    }

    size = write(fd[1], string, 14);

    if (size != 14) {
        printf("Can't write string!");
        exit (-1);
    }

    size = read(fd[0], resstring, size);

    if (size < 0) {
        printf("Can't read string!");
        exit (-1);
    }

    printf("Value from pipe: %s\n", resstring);
    if (close(fd[0]) < 0) {
        printf("Can't close input pipe!");
    }

    if (close(fd[1]) < 0) {
        printf("Can't close output pipe!");
    }

    return 0;
}
