#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{
    int fd1[2], fd2[2], result;
    size_t size;
    char resstring[14];

    if (pipe(fd1) < 0) {
        printf("Can't create first pipe!");
        exit (-1);
    }

    if (pipe(fd2) < 0) {
        printf("Can't create second pipe!");
        exit (-1);
    }

    result = fork();

    if (result < 0) {

        printf("Can't fork!");
        exit(-1);

    } else if (result > 0) {

        close(fd1[0]);
        close(fd2[1]);

        size = write(fd1[1], "Hello, world!", 14);

        if (size != 14) {
            printf("Can't write full string!\n");
            exit (-1);
        }

        size = read(fd2[0], resstring, 12);

        if (size != 12) {
            printf("Can't read parent full string!\n");
            exit (-1);
        }

        printf("[Parent] String value: %s\n", resstring);

        printf("Parent fork end!\n");

        close(fd1[1]);
        close(fd2[0]);

    } else {

        close(fd1[1]);
        close(fd2[0]);

        size = read(fd1[0], resstring, 14);

        if (size < 0) {
            printf("Can't read full string!\n");
            exit (-1);
        }

        printf("[Child] (1) String value: %s\n", resstring);

        size = write(fd2[1], "Child value", 12);

        if (size != 12) {
            printf("Can't write child full string!\n");
            exit (-1);
        }

        close(fd1[0]);
        close(fd2[1]);

        printf("Child fork end!\n");
    }

    return 0;
}
