#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

int main()
{
    int fd;
    size_t size;

    char resstring[] = "Fifo write data";
    char fifoName[] = "l8fifo.fifo";

    size_t message_size = 16;

    // (void)umask(0);

    if (mknod(fifoName, S_IFIFO | 0666, 0) < 0)  {

        // remove fifo
        unlink(fifoName);

        if (mknod(fifoName, S_IFIFO | 0666, 0) < 0) {
            printf("Can't create fifo!\n");
            exit(-1);
        }
    }

    if ((fd = open(fifoName, O_WRONLY)) < 0) {
        printf("Can't open file for write!\n");
        exit(-1);
    }

    size = write(fd, resstring, message_size);
    if (size != message_size) {
        printf("Error while writing...\n");
        exit(-1);
    }

    close(fd);

    return 0;
}
