#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

int main()
{
    int fd;
    size_t size;
    char fifoName[] = "l8fifo.fifo";

    char resstring[16];

    if ((fd = open(fifoName, O_RDONLY)) < 0) {
       printf("Can't open file for read!");
       exit(-1);
    }

    size = read(fd, resstring, 16);

    if (size < 0) {
        printf("Can't read full string!");
        exit(-1);
    }

    printf("String value: %s\n", resstring);

    close(fd);

    return 0;
}
