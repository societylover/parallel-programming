/* Программа с пользовательской обработкой сигнала SIGINT */

#include <signal.h>
#include <stdio.h>

/* Функция my_handler - пользовательский обработчик сигнала */

void my_handler(int nsig){

    printf("Receive signal ");

    if (nsig == SIGINT) {
        printf("CTRL-C pressed\n");
    }
    else if (nsig == SIGQUIT) {
        printf("CTRL-4 pressed\n");
    }

}

int main(void){

    /* Выставляем реакцию процесса на сигнал SIGINT */

    (void)signal(SIGINT, my_handler);

    /* Выставляем реакцию процесса на сигнал SIGQUIT */
    (void)signal(SIGQUIT, my_handler);

    /*Начиная с этого места, процесс будет печатать сообщение о возникновении сигнала SIGINT */

    while(1);
    return 0;

}
