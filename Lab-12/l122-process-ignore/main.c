/* Программа, игнорирующая сигнал SIGINT */

#include <signal.h>

int main(void){


/* Выставляем реакцию процесса на сигнал SIGQUIT на игнорирование */

(void)signal(SIGQUIT, SIG_IGN);

/*Начиная с этого места, процесс будет игнорировать возникновение сигнала SIGINT */

while(1);
return 0;

}
