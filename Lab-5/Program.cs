﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Lab5ShareBuffer
{
    class Program
    {
        static int bufferLength = 30;
        static int messagesCount = 1000;

        static string[] buffer = new string[bufferLength];

        static int iR = 0, iW = 0;
        static bool finish = false;
         

        static int readersN = 100, writersN = 50;
        static int[] ReadIndexCopy = new int[readersN], WriteIndexCopy = new int[writersN];

        static int[] ReadersPriority = new int[readersN], WritersPriority = new int[writersN];

        static bool bEmpty = true, bFull = false;

        static void readerThread(object state)
        {
            var states = state as object[];
            var readerI = (int)states[0];
            var evReadyToRead  = states[1] as ManualResetEventSlim;
            var evStartReading = states[2] as ManualResetEventSlim;

            var messages = new List<string>();

            Stopwatch sw = new Stopwatch();
            sw.Start();

            while (!finish)
            {
                evReadyToRead.Set(); 
                evStartReading.Wait();
                if (finish && evReadyToRead.IsSet) break;
                int index = ReadIndexCopy[readerI]; 
                messages.Add(buffer[index]); 
                bFull = false; 
                evStartReading.Reset(); 
            }

            sw.Stop();
            Console.WriteLine($"Читателем {readerI} было считано {messages.Count} сообщений");
            Console.WriteLine($"Читатель {readerI} затратил {sw.ElapsedMilliseconds}");
        }

        static void writerThread(object state)
        {
            var states = state as object[];
            var writerI = (int)states[0];
            var evReadyToWrite = states[1] as ManualResetEventSlim;
            var evStartWriting = states[2] as ManualResetEventSlim;

            Stopwatch sw = new Stopwatch();
            sw.Start();

            var messages = new string[messagesCount];
            for (int i = 0; i < messagesCount; i++)  messages[i] = $"thread {writerI} {i}";   

            int j = 0;
            while (j < messagesCount)
            {
                evReadyToWrite.Set();
                evStartWriting.Wait();
                int index = WriteIndexCopy[writerI]; 
                buffer[index] = messages[j++];  
                bEmpty = false; 
                evStartWriting.Reset();
            }

           sw.Stop();

           Console.WriteLine($"Писатель {writerI} записал {j} сообщений");
           Console.WriteLine($"Писатель {writerI} затратил {sw.ElapsedMilliseconds}");
        }

        static void manager()
        {
            var writers = new Thread[writersN];
            var readers = new Thread[readersN];

            ManualResetEventSlim[] evReadyToRead = new ManualResetEventSlim[readersN], evStartReading = new ManualResetEventSlim[readersN];
            ManualResetEventSlim[] evReadyToWrite = new ManualResetEventSlim[writersN], evStartWriting = new ManualResetEventSlim[writersN];

            for (int i = 0; i < readersN; i++)
            {
                evReadyToRead[i] = new ManualResetEventSlim(false);
                evStartReading[i] = new ManualResetEventSlim(false);
                readers[i] = new Thread(readerThread);
                readers[i].Start(new object[] { i, evReadyToRead[i], evStartReading[i] });
            }
            for (int i = 0; i < writersN; i++)
            {
                evReadyToWrite[i] = new ManualResetEventSlim(false);
                evStartWriting[i] = new ManualResetEventSlim(false);
                writers[i] = new Thread(writerThread);
                writers[i].Start(new object[] { i, evReadyToWrite[i], evStartWriting[i] });
            } 

            while (!finish)
            {
                if (!bFull)
                {
                    int iWriter = GetWriter(evReadyToWrite); 
                    if (iWriter != -1)
                    { 
                        evReadyToWrite[iWriter].Reset();
                        WriteIndexCopy[iWriter] = iW;
                        evStartWriting[iWriter].Set();
                        iW = (iW + 1) % bufferLength;
                        if (iW == iR) bFull = true;
                    }  
                }  
                if (!bEmpty)
                {
                    int iReader = GetReader(evReadyToRead);
                    if (iReader != -1)
                    { 
                        evReadyToRead[iReader].Reset();
                        ReadIndexCopy[iReader] = iR; 
                        evStartReading[iReader].Set();
                        iR = (iR + 1) % bufferLength;
                        if (iW == iR) bEmpty = true;
                    } 
                } 
                if (writers.All(i => !i.IsAlive) && bEmpty) finish = true;
            } 

            foreach (var sr in evStartReading.Where(i => !i.IsSet)) sr.Set();
             
            foreach (var reader in readers) reader.Join();  
        }

        static int GetWriter(ManualResetEventSlim[] evReadyToWrite)
        {
            List<int> freeWriters = new List<int>();
            for (int i = 0; i < writersN; i++)
            {
                if (evReadyToWrite[i].IsSet) freeWriters.Add(i);
            }
            if (freeWriters.Count == 0) return -1;
            return freeWriters.OrderBy(i => WritersPriority[i]).First();

        }

        static int GetReader(ManualResetEventSlim[] evReadyToRead)
        {
            List<int> freeReaders = new List<int>();
            for (int i = 0; i < readersN; i++)
            {
                if (evReadyToRead[i].IsSet) freeReaders.Add(i);
            }
            if (freeReaders.Count == 0) return -1;
            return freeReaders.OrderBy(i => ReadersPriority[i]).First();
        }

        static void Main(string[] args)
        {  
            var rand = new Random();
            for (int i = 0; i < writersN; i++) WritersPriority[i] = rand.Next(5); 
            for (int i = 0; i < readersN; i++) ReadersPriority[i] = rand.Next(5);

            manager();  
        }
    }
}
