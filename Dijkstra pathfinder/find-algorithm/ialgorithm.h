#ifndef IALGORITHM_H
#define IALGORITHM_H

#include "imeasurable.h"

// Класс-интерфейс для алгоритма по поиску путей в графе
// BFS, Dijkstra
class IAlgorithm : public IMeasurable
{
protected:
    resultVector *result;
    adjacencyList *vertices;

    void clearResult() {
        for (auto i : *result) delete i;
    }

    virtual void runAlgorithm(const vert start, const uint32_t index) = 0;
    virtual void intervalDivisionRun(const uint32_t left, const uint32_t right, uint32_t writeIndex);
    void saveToResult(const vert start, const vertList &p, const vertList &d, uint32_t index) noexcept;

public:
    IAlgorithm() = default;

    void setVertices(adjacencyList &adjacency) {
        vertices = &adjacency;
        if (result) {
            clearResult();
            delete result;
        }
        result = new resultVector(adjacency.size() * (adjacency.size()-1));
    }

    virtual void sequenceCalculate();
    virtual void parallelCalculate(uint8_t);

    void saveToFile(std::string_view file) {
        long ind = 0;
        std::ofstream fileResult (file.data());
        for (const auto &i : *result) {
            if (i != nullptr) {
                for (auto j : *i) fileResult << j << " ";
                fileResult << std::endl;
                ind++;
            }
        }
        fileResult.close();
    }
};

#endif // IALGORITHM_H
