#ifndef BFS_H
#define BFS_H

#include "ialgorithm.h"

class SPFA : public IAlgorithm
{
protected:
    void runAlgorithm(const vert start, const uint32_t index) override;
public:
    SPFA() = default;
};

#endif // BFS_H
