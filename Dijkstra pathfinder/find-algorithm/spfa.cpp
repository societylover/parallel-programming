#include "spfa.h"

void SPFA::runAlgorithm(const vert start, const uint32_t index)
{
    auto vertPointer = *vertices;
    auto vertCount = vertPointer.size();

    bool * isInQueue = new bool[vertCount] {false};
    isInQueue[start] = true;

    vertList d(vertCount, INF);
    vertList p(vertCount);

    d[start] = 0;

    std::queue<vert> watchingEdges;
    watchingEdges.push(start);

    while (!watchingEdges.empty()) {
        auto current = watchingEdges.front();
        watchingEdges.pop();
        isInQueue[current] = false;
        for (auto &i : vertPointer[current]) {
            if (d[i.first] > d[current] + i.second){
                d[i.first] = d[current] + i.second;
                p[i.first] = current;
                if (!isInQueue[i.first]) {
                    watchingEdges.push(i.first);
                    isInQueue[i.first] = true;
                }
            }
        }
    }

    delete[] isInQueue;

    // Нашли путь от одной точки до всех точек
    saveToResult(start, p, d, index);
}

