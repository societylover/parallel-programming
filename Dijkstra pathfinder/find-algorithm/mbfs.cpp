#include "mbfs.h"

void MBFS::runAlgorithm(const vert start, const uint32_t index)
{
    auto vertPointer = *vertices;
    for (auto i = 0; i < vertPointer.size(); i++) {
        if (i != start) {
            realization(start, i);
        }
    }
}

void MBFS::realization(const vert start, const vert end)
{
    auto vertPointer = *vertices;
    std::queue<vertList> pathResults;

    vertList path;
    path.push_back(start);

    pathResults.push(path);

    while (!pathResults.empty()) {
        path = pathResults.front();
        pathResults.pop();

        auto lastV = path.back();
        if (lastV == end) {
//            for (auto &i : path) {
//                std::cout << i << " ";
//            }
//            std::cout << '\n';
     }
        for (const auto &i : vertPointer[lastV]) {
            if (std::find(path.begin(), path.end(), i.first) == std::end(path)) {
                vertList newpath(path);
                newpath.push_back(i.first);
                pathResults.push(newpath);
            }
        }
    }
}
