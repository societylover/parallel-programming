#include "ialgorithm.h"

void IAlgorithm::intervalDivisionRun(const uint32_t left, const uint32_t right, uint32_t writeIndex)
{
    for (auto i = left; i < right; i++) {
        runAlgorithm(i, writeIndex);
        writeIndex += vertices->size() - 1;
    }
}

void IAlgorithm::saveToResult(const vert start, const vertList &p, const vertList &d, uint32_t index) noexcept
{
    for (auto i = 0; i < d.size(); i++) {
        if (d[i] == INF || d[i] == 0) continue;
        vertResult *currentResult = new vertResult();
        currentResult->push_back(start);
        currentResult->push_back(i);
        std::stack<vert> path;
        for (auto v = i; v != start; v = p[v]) path.push(v);
        while (path.size() != 1) {
            currentResult->push_back(path.top());
            path.pop();
        }
        currentResult->push_back(d[i]);
        result->operator[](index++) = currentResult;
    }
}

void IAlgorithm::sequenceCalculate()
{
    clearResult();
    startTimer();
    intervalDivisionRun(0, vertices->size(), 0);
    stopTimer();
}

void IAlgorithm::parallelCalculate(uint8_t threadN)
{
    clearResult();

    startTimer();

    std::vector<std::thread> workers;

    uint32_t step = vertices->size() / threadN;

    uint16_t currentIndex = 0;
    uint16_t rightEdge = step + (vertices->size() % threadN);
    uint32_t index = 0;

    for (auto i = 0; i < threadN; i++) {
        auto thread = std::thread([=](){ intervalDivisionRun(currentIndex, rightEdge, index); });
        index += (rightEdge - currentIndex) * (vertices->size()-1);
        currentIndex = rightEdge;
        rightEdge += step;

        workers.push_back(std::move(thread));
    }

    for (auto& i: workers) i.join();

    stopTimer();
}
