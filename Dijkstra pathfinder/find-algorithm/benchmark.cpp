#include "benchmark.h"
#include "vertexReader.h"

void benchmark::runShortPathBenchmark() const noexcept
{
    basicBenchmarkRunner(shortPaths, graphForShortPaths);
}

void benchmark::runAllPathBenchmark() const noexcept
{
    basicBenchmarkRunner(allPaths, graphForAllPaths);
}

void benchmark::basicBenchmarkRunner(const visualizeList &algos, const graphFiles &files) const noexcept
{
    for (auto &i : files) {
        auto graph = readAndFill(i);
        std::cout << "\n[File]: " << i << std::endl;
        for (auto &algo : algos) {
            algo.first->setVertices(graph);
            std::cout << "Algo: " << algo.second << std::endl;
            std::cout << "1 thread: ";
            algo.first->sequenceCalculate();
            std::cout << algo.first->getTimeSpent() << "\n";
            for (auto step = 2; step <= MAX_THREAD_COUNT; step+=2) {
                std::cout << step <<" thread: ";
                algo.first->parallelCalculate(step);
                std::cout << algo.first->getTimeSpent() << "\n";
            }
        }
    }
}

