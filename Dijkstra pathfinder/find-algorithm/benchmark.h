#ifndef BENCHMARK_H
#define BENCHMARK_H

#include "ialgorithm.h"

#if defined (__APPLE__)
    constexpr uint8_t MAX_THREAD_COUNT = 8;
#else
    constexpr uint8_t MAX_THREAD_COUNT = 12;
#endif

using visualPair = std::pair<IAlgorithm*, std::string_view>;
using visualizeList = std::vector<visualPair>;

using graphFiles = std::vector<std::string_view>;

class benchmark
{
public:
    benchmark() = default;

    void setShortPathAlgoritms(const std::initializer_list<visualPair> &list) {
        for (const auto &i : list)
            shortPaths.push_back(std::move(i));
    }

    void setAllPathsAlgoritms(const std::initializer_list<visualPair> &list) {
        for (const auto &i : list)
            allPaths.push_back(std::move(i));
    }

    void setFilesForShortPaths(const std::initializer_list<string_view> &files) {
        for (const auto &i : files)
            graphForShortPaths.push_back(std::move(i));
    }

    void setFilesForAllPaths(const std::initializer_list<string_view> &files) {
        for (const auto &i : files)
            graphForAllPaths.push_back(std::move(i));
    }

    void runShortPathBenchmark() const noexcept;
    void runAllPathBenchmark() const noexcept;

private:
    visualizeList shortPaths, allPaths;
    graphFiles graphForShortPaths, graphForAllPaths;

    void basicBenchmarkRunner(const visualizeList&, const graphFiles&) const noexcept;
};

#endif // BENCHMARK_H
