#ifndef MBFS_H
#define MBFS_H

#include "ialgorithm.h"

class MBFS : public IAlgorithm
{
protected:
    void runAlgorithm(const vert start, const uint32_t index);

private:
    void realization(const vert start, const vert end);
};

#endif // MBFS_H
