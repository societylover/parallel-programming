#include "dijkstra.h"

void Dijkstra::runAlgorithm(const vert start, const uint32_t index)
{
    auto vertPointer = *vertices;
    auto vertCount = vertPointer.size();

    vertList d(vertCount, INF);
    vertList p(vertCount);

    d[start] = 0;

    std::set<priceVert> unusedSet;
    unusedSet.insert(std::make_pair(0, start));

    while (!unusedSet.empty()) {
        auto top = *unusedSet.begin();
        unusedSet.erase(unusedSet.begin());
        for (auto i : vertPointer[top.second]) {
            if (d[i.first] > d[top.second] + i.second) {
                unusedSet.erase(std::make_pair(d[i.first], i.first));
                d[i.first] = d[top.second] + i.second;
                unusedSet.insert(std::make_pair(d[i.first], i.first));

                p[i.first] = top.second;
            }
        }
    }

    saveToResult(start, p, d, index);
}
