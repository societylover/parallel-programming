#ifndef VERTEXREADER_H
#define VERTEXREADER_H

#include "usings.h"

adjacencyList readAndFill(std::string_view filename) {
    std::fstream file(filename.data());

    std::stringstream ss;
    std::string CountS = "";
    std::getline(file, CountS);

    uint32_t vertCount;
    ss << CountS;
    ss >> vertCount;
    ss.clear();

    std::string tempStr = "";

    adjacencyList adjacency(vertCount);

    uint32_t i = 0;
    // Запоминаем данные из файла в ajacencyList
    while (std::getline(file, tempStr)) {
        ss << tempStr;
        vertPrice vert;
        while (ss >> vert.first >> vert.second)
            adjacency[i].push_back(vert);

        ss.clear();
        i++;
    }
    return adjacency;
}

#endif // VERTEXREADER_H
