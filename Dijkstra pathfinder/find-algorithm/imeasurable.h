#ifndef IMEASURABLE_H
#define IMEASURABLE_H

#include "usings.h"

class IMeasurable
{
    system_clock::time_point timer;
    duration<double, std::milli> timespent;

public:
    IMeasurable() = default;
    void startTimer() noexcept {
   // #if defined (__APPLE__)
        timer = std::chrono::system_clock::now();
//    #elif defined(_WIN64)
//        timer = high_resolution_clock::now();
//    #endif
    }

    void stopTimer() noexcept {
    //#if defined (__APPLE__) || (__linux__) || (__unix__)
        timespent = std::chrono::system_clock::now() - timer;
    //#elif defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__) || defined(_WIN64)
       // timespent = std::chrono::duration<double, std::milli>(high_resolution_clock::now() - timer);
    //#endif

    }

    double getTimeSpent() const noexcept {
        return timespent.count();
    }
};

#endif // IMEASURABLE_H
