#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include "ialgorithm.h"

class Dijkstra : public IAlgorithm
{
protected:
    void runAlgorithm(const vert start, const uint32_t index) override;
public:
    Dijkstra() = default;

};

#endif // DIJKSTRA_H
