#ifndef VISUALDEMONSTRATION_H
#define VISUALDEMONSTRATION_H

#include "ialgorithm.h"

//using visualPair = std::pair<IAlgorithm*, std::string_view>;
//using visualizeList = std::vector<visualPair>;

class VisualDemonstration
{
//    visualizeList algorithms;

//public:
//    VisualDemonstration(const std::initializer_list<visualPair> &list) {
//        for (const auto &i : list)
//            algorithms.push_back(std::move(i));
//    };

//    void show () {
//        int selectedVariant = 0;
//        std::cout << "Выберите алгоритм:" << std::endl;
//        for (auto i = 0; i < algorithms.size(); i++) std::cout << i <<" < " << algorithms[i].second << std::endl;
//        std::cout << "> ";
//        std::cin >> selectedVariant;
//        if (selectedVariant >= algorithms.size()) return;
//        auto selectedAlg = &algorithms[selectedVariant];
//        std::cout << "\n1  < Последовательная реализация (без потоков)" << std::endl;
//        std::cout << "2+ < С использованием потоков (введите количество)" << std::endl;
//        std::cin >> selectedVariant;

//        if (selectedVariant == 0) return;

//        if (selectedVariant == 1) selectedAlg->first->sequenceCalculate();
//        else selectedAlg->first->parallelCalculate(selectedVariant);

//        std::cout << "\nВремя затрачено: " << selectedAlg->first->getTimeSpent() << " мс."<< std::endl;

//        selectedAlg->first->saveToFile("test.txt");
//    }
};

#endif // VISUALDEMONSTRATION_H
