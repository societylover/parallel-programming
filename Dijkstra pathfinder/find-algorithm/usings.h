#ifndef USINGS_H
#define USINGS_H

#include <iostream>
// Для реализации алгоритма подсчета
#include <vector>
#include <stack>
#include <set>
#include <queue>
// Для потокобезопасности и устранения критической секции
#include <atomic>
#include <fstream>
#include <string>
#include <sstream>
// Лишь для установки названия результирующего файла
#include <regex>
// Работа с потоками
#include <thread>

// Для замера времени
using namespace std::chrono;

using namespace std;

using vert = uint16_t;
using vertList = std::vector<uint16_t>;
using vertPrice = std::pair<vert, uint32_t>;
using priceVert = std::pair<uint8_t, vert>;
using vertNeighbours = std::vector<vertPrice>;
using adjacencyList = std::vector<vertNeighbours>;

using vertResult = std::vector<uint32_t>; // Возврат результата в виде: вершина-старт вершина-конец путь, ..., путь цена

using usedList = std::vector<uint8_t>;

constexpr uint32_t INF = UINT16_MAX;

using resultVector = std::vector<vertResult*>;


#endif // USINGS_H
