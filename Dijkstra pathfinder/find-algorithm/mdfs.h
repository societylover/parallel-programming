#ifndef MDFS_H
#define MDFS_H

#include "ialgorithm.h"

class MDFS : public IAlgorithm
{
protected:
    void runAlgorithm(const vert start, const uint32_t index);

private:
    void DFS(const vert start, const vert end,  std::vector<bool> &visited, vertList &path, vert &path_index);
};

#endif // MDFS_H
