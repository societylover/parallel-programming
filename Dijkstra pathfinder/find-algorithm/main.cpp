#include "benchmark.h"

#include "dijkstra.h"
#include "mbfs.h"
#include "spfa.h"
#include "mdfs.h"

int main(int argc, char **argv)
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__) || defined(_WIN64)
        system("chcp 65001>nul");
    #endif

    benchmark bench;

    auto dijk = Dijkstra();
    auto spfa = SPFA();

    bench.setShortPathAlgoritms({
        std::make_pair(&dijk, "Алгоритм Дейкстры - поиск минимального пути"),
        std::make_pair(&spfa, "SPFA - Shortest Path Faster Algorithm - поиск минимального пути")
    });

    bench.setFilesForShortPaths({"1000.txt", "2000.txt", "3000.txt", "7000.txt"});

    bench.runShortPathBenchmark();

    auto mbfs = MBFS();
    auto mdfs = MDFS();

    bench.setAllPathsAlgoritms({
        //std::make_pair(&mbfs, "MBFS - поиск всех путей"),
        std::make_pair(&mdfs, "MDFS - поиск всех путей")
    });

    bench.setFilesForAllPaths({ "12.txt", "13.txt", "14.txt", "15.txt", "17.txt"}); //, "20.txt"*/ }); //, "13.txt", "15.txt"

    bench.runAllPathBenchmark();

    return 0;
}
