#include "mdfs.h"

void MDFS::runAlgorithm(const vert start, const uint32_t index)
{
    auto vertPointer = *vertices;
    for (auto i = 0; i < vertPointer.size(); i++) {
        if (i != start) {
            std::vector<bool> visited(vertPointer.size(), false);
            vertList path(vertices->size());
            vert path_index = 0;
            DFS(start, i, visited, path, path_index);
        }
    }
}

void MDFS::DFS(const vert start, const vert end, std::vector<bool> &visited, vertList &path, vert &path_index)
{
    visited[start] = true;
    path[path_index] = start;
    path_index++;

    if (start == end) {
//            for (int i = 0; i < path_index; i++)
//                cout << path[i] << " ";
//            cout << endl;
    }
    else
    {
        for (const auto &i : vertices->operator[](start)) {
            if (!visited[i.first])
                DFS(i.first, end, visited, path, path_index);
            }
    }

    path_index--;
    visited[start] = false;
}

