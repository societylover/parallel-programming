#include <iostream>
#include <vector>
#include <fstream>
#include <numeric>
#include <algorithm>
#include <time.h>
#include <random>

using namespace std;

constexpr uint8_t minRandom = 1;
constexpr uint8_t maxRandom = 10;


int main(int argc, char **argv)
{
    if (argc == 0) return 0;

    srand(time(0));

    uint32_t elementCount = stoi(argv[1]);

    std::vector<uint32_t> vertRandomed(elementCount);

    std::iota(vertRandomed.begin(), vertRandomed.end(), 0);
    std::random_shuffle(vertRandomed.begin(), vertRandomed.end());

    std::vector<std::vector<uint32_t>> result(elementCount,
                                              std::vector<uint32_t>(elementCount, 0));

    // Минимальное связывание
    std::mt19937 gen(time(0));
    std::uniform_int_distribution<uint8_t> necessaryConnection(minRandom, maxRandom);

    for (auto i = 0; i < vertRandomed.size()-1; i++)
        result[vertRandomed[i]][vertRandomed[i+1]] = necessaryConnection(gen);

    std::uniform_int_distribution<int> randomlyConnection(-maxRandom, maxRandom);

    std::ofstream graphFile(std::to_string(elementCount) + ".txt");
    graphFile << elementCount << " " << std::endl;

    for (auto i = 0; i < result.size(); i++) {
        for (auto j = 0; j < result.size(); j++) {
            if (result[i][j] == 0 && i != j) {
                auto isConnection = randomlyConnection(gen);
                if (isConnection >= minRandom && result[j][i] == 0) result[i][j] = isConnection;
            }
            if (result[i][j] != 0) graphFile << j << " " << result[i][j] << " ";
        }
        graphFile << "\n";
    }

    graphFile.close();

    return 0;
}
