#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main()
{
    char *shm_value_text;
    int text_size = 33;
    int shmid;
    char *shm_filename = "test.shm";

    key_t key;

    if ((key = ftok(shm_filename, 0)) < 0) {
        printf("Error while get key!");
        exit (-1);
    }

    if ((shmid = shmget(key, text_size * sizeof (char), 0)) < 0) {
                printf("Error while get shmid of exist shared memory");
                exit (-1);
    }

    if((shm_value_text = (char *)shmat(shmid, NULL, 0)) == (char *)(-1)){

    printf("Can't attach shared memory\n");
    exit(-1);

    }

    printf("Stored value is: %s\n", shm_value_text);

    if (shmdt(shm_value_text) < 0) {
        printf("Can't detach shared memory value!");
        exit (-1);
    }

    if (shmctl(shmid, IPC_RMID, NULL) < 0) {
        printf("Can't remove shared memory!");
        exit (-1);
    }

    return 0;
}
