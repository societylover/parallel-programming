#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main()
{
    char *shm_text = "Текст из l91-shm-writerasa\0";
    int text_size = strlen(shm_text);

    char *shm_value_text;
    int shmid;
    char *shm_filename = "test.shm";

    key_t key;

    if ((key = ftok(shm_filename, 0)) < 0) {
        printf("Error while get key!");
        exit (-1);
    }

    if ((shmid = shmget(key, text_size * sizeof(char), 0666|IPC_CREAT|IPC_EXCL)) < 0) {

        if(errno != EEXIST){

        printf("Can\'t create shared memory\n");
        exit(-1);
    }
    }

    if((shm_value_text = (char *)shmat(shmid, NULL, 0)) == (char *)(-1)){

    printf("Can't attach shared memory\n");
    exit(-1);

    }

    strlcpy(shm_value_text, shm_text, text_size+1);

    if (shmdt(shm_value_text) < 0) {
        printf("Can't detach shared memory value!");
        exit (-1);
    }

    return 0;
}
