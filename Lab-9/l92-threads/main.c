/* Программа 2 для иллюстрации работы с разделяемой памятью*/

/* Мы организуем разделяемую память для массива из 3-х целых чисел. Первый элемент массива является счетчиком числа запусков программы 1,  второй элемент массива - счетчиком числа запусков программы 2, т. е. данной программы, третий элемент массива - счетчиком числа запусков обеих программ */

#include <sys/shm.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <pthread.h>

// Добавим +1 к счетчику
void* addCounter(void* args) {
    *((int*)args) += 1;
    return NULL;
}

pthread_t mythid; /* Для идентификатора нити исполнения */

int main()
{

int *array;
int shmid;
int new = 1;

char pathname[] = "values.shm"; /* Имя файла, использующееся для генерации ключа. Файл с таким именем должен существовать в текущей директории */

key_t key; /* IPC ключ */

if((key = ftok(pathname,0)) < 0){

    printf("Can\'t generate key\n");
    exit(-1);

}

if((shmid = shmget(key, 3*sizeof(int), 0666|IPC_CREAT|IPC_EXCL)) < 0){

    if(errno != EEXIST){

    printf("Can\'t create shared memory\n");
    exit(-1);

    } else {

        if((shmid = shmget(key, 3*sizeof(int), 0)) < 0){

        printf("Can\'t find shared memory\n");
        exit(-1);

        }
    new = 0;

    }
}

if((array = (int *)shmat(shmid, NULL, 0)) == (int *)(-1)){

    printf("Can't attach shared memory\n");
    exit(-1);
}

pthread_t secondth, thirdth;
int result;

if(new){

    array[0] = 0;
    array[1] = 1;
    array[2] = 1;

} else {

    result = pthread_create(&secondth, (pthread_attr_t *)NULL, addCounter, (void*)&array[0]);

    if(result != 0){

        printf ("Error on second thread create, return value = %d\n", result);
        exit(-1);

    }

    result = pthread_create(&thirdth, (pthread_attr_t *)NULL, addCounter, (void*)&array[1]);

    if(result != 0){

        printf ("Error on third thread create, return value = %d\n", result);
        exit(-1);

    }
    pthread_join(secondth, (void **)NULL);
    pthread_join(thirdth, (void **)NULL);
    array[2] += 1;
}



printf("Program 1 was spawn %d times, program 2 - %d times, total - %d times\n",
array[0], array[1], array[2]);

if(shmdt(array) < 0){
printf("Can't detach shared memory\n");
exit(-1);

}
return 0;

}
