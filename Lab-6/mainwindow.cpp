#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QTimer>
#include <QVector>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QStandardItemModel * test = new QStandardItemModel(65, 100);
    ui->tableView->setModel(test);
    dotsNewStatus = new QVector<QVector<uint8_t>>(test->rowCount(),
                                                  QVector<uint8_t>(test->columnCount()));

    msg = new QMessageBox(QMessageBox::Warning, "Конец игры", "Игра окончена");

    timer = new QTimer();
    timer->callOnTimeout(std::bind(&MainWindow::updateLifeGame, this));
    timer->setInterval(500);

    ui->tableView->selectionModel()->select(ui->tableView->model()->index(39, 30), QItemSelectionModel::Select);
    ui->tableView->selectionModel()->select(ui->tableView->model()->index(38, 30), QItemSelectionModel::Select);
    ui->tableView->selectionModel()->select(ui->tableView->model()->index(37, 30), QItemSelectionModel::Select);
    ui->tableView->selectionModel()->select(ui->tableView->model()->index(36, 31), QItemSelectionModel::Select);

    ui->tableView->selectionModel()->select(ui->tableView->model()->index(37, 32), QItemSelectionModel::Select);
    ui->tableView->selectionModel()->select(ui->tableView->model()->index(38, 32), QItemSelectionModel::Select);
    ui->tableView->selectionModel()->select(ui->tableView->model()->index(39, 32), QItemSelectionModel::Select);
    ui->tableView->selectionModel()->select(ui->tableView->model()->index(40, 31), QItemSelectionModel::Select);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void delay()
{
    QTime dieTime = QTime::currentTime().addSecs(1);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

void MainWindow::on_next_clicked()
{
    ui->next->hide();
    timer->start();
    updateLifeGame();
}

uint8_t MainWindow::dotPotencial(QModelIndex index) const noexcept
{
    auto potencial = 0;
    auto dotRow = index.row(), dotColumn = index.column();
    for (auto topRow = dotRow-1; topRow <= dotRow + 1; topRow++)
        for (auto topCol = dotColumn-1; topCol <= dotColumn + 1; topCol++)
                if (ui->tableView->selectionModel()->isSelected(ui->tableView->model()->index(topRow, topCol))) potencial++;

    if (ui->tableView->selectionModel()->isSelected(ui->tableView->model()->index(dotRow, dotColumn))) potencial--;

    return potencial;
}

void MainWindow::calculateAsync() noexcept
{
    QThreadPool pool;

    for (auto i= 0; i < 10; i++) pool.reserveThread();

    for (auto i = 0; i < dotsNewStatus->size(); i++) {
        for (auto j = 0; j < (*dotsNewStatus)[0].size(); j++) {
            auto future = QtConcurrent::task([=](int i, int j){
                auto dot = ui->tableView->model()->index(i, j);
                return dotPotencial(dot);
            }).withArguments(i, j).onThreadPool(pool).spawn();
            (*dotsNewStatus)[i][j] = future.result();
        }
    }
    pool.waitForDone();
    for (auto i= 0; i < 10; i++) pool.releaseThread();
}

void MainWindow::calculateRowsAsync() noexcept
{
    QThreadPool pool;
    for (auto i= 0; i < 10; i++) pool.reserveThread();
    for (auto i = 0; i < dotsNewStatus->size(); i++) {

            auto future = QtConcurrent::task([=](int i){
                QVector<uint8_t> result(dotsNewStatus[0].size());
                for (auto j = 0; j < (*dotsNewStatus)[0].size(); j++) {
                auto dot = ui->tableView->model()->index(i, j);
                result[j] = dotPotencial(dot); }
                return result;
            }).withArguments(i).onThreadPool(pool).spawn();
            (*dotsNewStatus)[i] = future.result();
    }
    pool.waitForDone();
    for (auto i= 0; i < 10; i++) pool.releaseThread();
}

void MainWindow::calculateSync() noexcept
{
    for (auto i = 0; i < dotsNewStatus->size(); i++) {
        for (auto j = 0; j < (*dotsNewStatus)[0].size(); j++) {
            auto dot = ui->tableView->model()->index(i, j);
            (*dotsNewStatus)[i][j] = dotPotencial(dot);
        }
    }
}

QItemSelectionModel::SelectionFlag MainWindow::selectStatus(QModelIndex item)
{
    return (*dotsNewStatus)[item.row()][item.column()] == 3 ? QItemSelectionModel::Select :
            ((*dotsNewStatus)[item.row()][item.column()] < 2 || (*dotsNewStatus)[item.row()][item.column()] > 3) ?
            QItemSelectionModel::Deselect : ui->tableView->selectionModel()->isSelected(item) ?  QItemSelectionModel::Select : QItemSelectionModel::Deselect;
}

void MainWindow::updateLifeGame()
{
    // delay();
    calculateAsync();
    lastSelectedState.clear();
    for (auto i = 0; i < dotsNewStatus->size(); i++) {
        for (auto j = 0; j < (*dotsNewStatus)[0].size(); j++) {
            auto dot = ui->tableView->model()->index(i, j);
            auto selectedStatus = selectStatus(dot);
            if (selectedStatus == QItemSelectionModel::Select) lastSelectedState.push_back(dot);
            ui->tableView->selectionModel()->select(dot, selectedStatus);
        }
    }
    checkForEnd();
}

void appEnd(QMessageBox *msg, QTimer *timer) {
    timer->stop();
    msg->exec();
    QApplication::exit();
}

void MainWindow::checkForEnd()
{
    if (!ui->tableView->selectionModel()->hasSelection()) {
        msg->setText("Не осталось живых клеток!");
        appEnd(msg, timer);
    } else if (prevStates.indexOf(lastSelectedState) != -1) {
        msg->setText("Повтор состояния!");
        appEnd(msg, timer);
    }

    prevStates.push_back(lastSelectedState);
}

void MainWindow::on_cancel_clicked()
{
    timer->stop();
    if (prevStates.empty()) lastSelectedState = prevStates.pop();
    for (auto &i : lastSelectedState) {
        ui->tableView->selectionModel()->select(i, QItemSelectionModel::Select);
    }
    timer->start();
}
