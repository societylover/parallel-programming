#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QStandardItemModel>
#include <QtConcurrent/QtConcurrent>
#include <QMessageBox>
#include <QStack>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_next_clicked();
    void on_cancel_clicked();

private:
    Ui::MainWindow *ui;
    QVector<QVector<uint8_t>> * dotsNewStatus;
    QVector<const QModelIndex *> selected;

    QItemSelectionModel::SelectionFlag selectStatus(QModelIndex item);

    QTimer * timer;

    void updateLifeGame();

    void checkForEnd();
    QMessageBox * msg;

    QStack <QVector<QModelIndex>> prevStates;
    QVector<QModelIndex> lastSelectedState;

    uint8_t dotPotencial (QModelIndex) const noexcept;
    void calculateAsync() noexcept;
    void calculateRowsAsync() noexcept;
    void calculateSync() noexcept;
};
#endif // MAINWINDOW_H
